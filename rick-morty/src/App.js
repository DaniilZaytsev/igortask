import React from 'react'
import './App.css';
import Header from './components/Header/Header';
import Main from './components/Main/main';

const App =()=> {
  return (
    <div className="app-wrapper">
      
      <Header/>
      <Main/>
    </div>
    
    
  );
}

export default App;
