import React from "react";
import s from './CharCard.module.css'

const CharList=()=>[{
name='Rick',
status   ='dead',

episode ='221'
}]
const CharCard = (props) =>{
    return(
        <div className= {s.CharCards}>
            
            {/* <img>{props.image}</img> */}
            <div className={s.LinkName}>
                <a href="">{props.name}</a>
            </div>
            <div className={s.status}>
                <span>{props.status}</span>
            </div>
            <div className={s.episode}>
                <span >Last known location</span>
            </div>
            <div className={s.LocationName}>
                <a href="">{props.location}</a>
            </div>
            <div className={s.episode}>
                <span>First see in</span>
            </div>
            <div className={s.EpisodeName}>
                <a href="">{props.episode}</a>
            </div>
    </div>
    )
}  
export default CharCard;